#include "pch.h"
#include "Wizard.h"


Wizard::Wizard()
{
	mHP = 100;
	mMana = 100;
}


Wizard::~Wizard()
{
}

string Wizard::getName(string name)
{
	mName = name;
	return mName;
}

int Wizard::getHP()
{
	return mHP;
}

int Wizard::getMana()
{
	return mMana;
}

void Wizard::printStats()
{
	cout << "Name = " << mName << endl;
	cout << "HP = " << mHP << endl;
	cout << "MP = " << mMana << endl;
}

void Wizard::attack(Spell * magic, Wizard* target)
{
	srand(time(NULL));
	
	int DMG = rand() % 10;
	
	cout << mName << " cast " << magic->getSpellName() << endl;
	mMana -= magic->getManaCost();

	int Damage = magic->getSpellDamage(DMG);
	if (Damage < 1)
	{
		Damage = 1;
	}
	cout << "It deals " << Damage << " damage " << endl;

	target->mHP -= Damage;
	
}


