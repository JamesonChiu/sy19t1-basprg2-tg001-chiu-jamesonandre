#pragma once
#include <string>
#include <iostream>
#include "Spell.h"
using namespace std;

class Wizard
{
public:
	Wizard();
	~Wizard();

	string getName(string name);
	int getHP();
	int getMana();

	//Action
	void printStats();
	void attack(Spell* magic, Wizard* target);
	
	

private:
	string mName;
	int mHP;
	int mMana;
	
	Spell* mMagic;

};

