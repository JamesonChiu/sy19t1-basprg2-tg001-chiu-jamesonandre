
#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include "Wizard.h"
#include "Spell.h"
using namespace std;


int main()
{
	bool x = 1;

	string name;
	Wizard *w1 = new Wizard;
	Spell *m1 = new Spell;
	Wizard *w2 = new Wizard;
	Spell *m2 = new Spell;

	cout << "Input Name: " << endl;
	cin >> name;
	w1->getName(name);
	cin >> name;
	w2->getName(name);

	cout << "Here are the details: " << endl;
	w1->printStats();
	w2->printStats();

	_getch();
	system("cls");

	cout << "Let the spell fight begin" << endl;

	while (true)
	{
		if (w1->getHP() <= 0 || w1->getMana() <= 0)
		{
			cout << "Game Over" << endl;
			cout << "Wizard 2 wins" << endl;
			false;
		}
		else if (w2->getHP() <= 0 || w2->getMana() <= 0)
		{
			cout << "Game Over" << endl;
			cout << "Wizard 1 wins" << endl;
			false;
		}
		else
		{
			true;
		}

		cout << "What will you do?" << endl;
		cout << "1.) Attack" << endl;
		cout << "2.) Print Stats" << endl;

		int ans;
		cin >> ans;
		system("cls");

		switch (ans)
		{
		case 1:
			w1->attack(m1, w2);
			_getch();
			system("cls");
			w2->attack(m2, w1);
			break;

		case 2:
			w1->printStats();
			w2->printStats();
			break;
		}
	
		_getch();
		system("cls");
	}
	system("cls");
}

