#pragma once
#include "string"
#include <time.h>
using namespace std;

class Spell
{
public:
	Spell();
	~Spell();

	string getSpellName();
	int getManaCost();
	int getSpellDamage(int DMG);

private:
	string spellName;
	int manaCost;
	int spellDamage;
};

