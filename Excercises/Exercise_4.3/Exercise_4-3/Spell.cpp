#include "pch.h"
#include "Spell.h"


Spell::Spell()
{
	srand(time(NULL));
	spellName = "FireBall";
	manaCost = 10;
}


Spell::~Spell()
{
}

string Spell::getSpellName()
{
	return spellName;
}

int Spell::getManaCost()
{
	return manaCost;
}

int Spell::getSpellDamage(int DMG)
{
	spellDamage = DMG;
	return spellDamage;
}
