// Exercise_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
using namespace std;

int solveFactorial(int input)
{
	int product = 1;

	for (int value = 1; value <= input; value++)
	{
		product = product * value;
	}

	return product;
}

void printArray(string items[8])
{
	

	for (int i = 0; i < 8; i++)
	{
		cout << items[i] << " ";
		
	}

}

void countArray(string items[8])
{
	

	for (int i = 0; i <= 8; i++)
	{
		if (i == 8)
		{
			cout << "There are " << i  << " items" << endl;
		}

	}
}

void arrayFill(int array[10])
{
	srand(time(NULL));

	for (int i = 0; i < 10; i++)
	{
		array[i] = rand() % 100;
		cout << array[i] << " ";
	}

	cout << endl;
}

int largestValue(int array[10])
{
	int large = array[0]; // array at 0

	for (int i = 0; i < 10; i++)
	{
		if (array[i] > large) // if the array at position i is greater than the value of the array at position 0
		{
			large = array[i]; // then the value of large will become the value of the array at the postition i
		}
	}

	return large;
}

void sortArray(int array[10]) // using the array of no. 4 in acesnding form
{
	int temp; // use to temporary store the value

	for (int i = 0; i < 10; i++)
	{
		for (int i2 = i+1; i2< 10;i2++) // ahead of the outer loop by 1
		{
			if (array[i] > array[i2]) //array with the value of i is greater than array with the value of i2
			{
				temp = array[i];		//temp will store the integer of the array with the value of i
				array[i] = array[i2];	//array with the value of i will become the value of i2
				array[i2] = temp;
			}
			
			
		}
		cout << array[i] << endl;
	}
	
	
}

int main()
{
	// Factorial
	int Input;
	cout << "Input Number" << endl;
	cin >> Input;
	cout << "Your Factorial is " << solveFactorial(Input) << endl;
	system("pause");
	system("cls");

	//Print Array
	string items[8] = { "Red_Potion", "Blue_Potion", "Yggdrasil_Leaf", "Elixir", "Teleport_Scroll", "Red_Potion", "Red_Potion", "Elixir" };
	printArray(items);
	_getch();
	system("cls");

	//Number Of Instances
	countArray(items);
	_getch();
	system("pause");
	system("cls");

	//RNG Array Fill
	int Number[10] = {};
	arrayFill(Number);

	//Largest Array Value
	int MaxValue;
	cout << "The largest value of the array is "<< largestValue(Number) << endl;

	//Sort Array
	cout << "By Acending Order" << endl;
	sortArray(Number);

	_getch();
	return 0;

}

