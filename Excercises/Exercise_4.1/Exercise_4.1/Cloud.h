#pragma once
#include <string>
#include <iostream>
using namespace std;

class Cloud
{
public:
	string name;
	int level;
	int HP;
	int MP;
	int EXP;
	int next_level;
	int limit_level;
	int limit_gauge;
	int action_time;
	int strenght;
	int dexterity;
	int vitality;
	int magic;
	int spirit;
	int luck;
	int attack;
	int attack_percent;
	int defence;
	int defence_percent;
	int magic_attack;
	int magic_defence;
	int magic_defence_percent;
	string skill_name;
	string weapon;
	string armor;
	string accesories;
	string materia;

	void walk();
	void attack();
	void Limit_Skill();
	void Magic();
	void summon();
	void item();
	void defence();
	void run();
	void equip();
	void interact();
	void purchase();
	
};

