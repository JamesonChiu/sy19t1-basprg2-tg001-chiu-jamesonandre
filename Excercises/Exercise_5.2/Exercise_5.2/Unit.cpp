#include "pch.h"
#include "Unit.h"

Unit::Unit()
{
	mHP = 50;
	mPOW = 1;
	mDEX = 1;
	mVIT = 1;
	mAGI = 1;
}


Unit::~Unit()
{
}

void Unit::displayStats()
{
	cout << "HP: " << mHP << endl;
	cout << "POW: " << mPOW << endl;
	cout << "DEX: " << mDEX << endl;
	cout << "VIT: " << mVIT << endl;
	cout << "AGI: " << mAGI << endl;
}

void Unit::setHP(int hp)
{
	mHP = hp;
}

void Unit::setPOW(int pow)
{
	mPOW = pow;
}

void Unit::setDEX(int dex)
{
	mDEX = dex;
}

void Unit::setVIT(int vit)
{
	mVIT = vit;
}

void Unit::setAGI(int agi)
{
	mAGI = agi;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getPOW()
{
	return mPOW;
}

int Unit::getDEX()
{
	return mDEX;
}

int Unit::getVIT()
{
	return mVIT;
}

int Unit::getAGI()
{
	return mAGI;
}

int Unit::useSkill()
{
	return 0;
}
