#pragma once
#include <iostream>
#include <string>
using namespace std;

class Unit
{
public:
	Unit();
	~Unit();

	void displayStats();

	void setHP(int hp);
	void setPOW(int pow);
	void setDEX(int dex);
	void setVIT(int vit);
	void setAGI(int agi);
	
	int getHP();
	int getPOW();
	int getDEX();
	int getVIT();
	int getAGI();

	virtual int useSkill();

private:
	int mHP;
	int mPOW;
	int mDEX;
	int mVIT;
	int mAGI;
};

