// Exercise_5.2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <vector>
#include "Unit.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"
#include <time.h>
using namespace std;

void applySkill(vector <Unit*> skills, int index)
{
	if (index == 1)
	{
		Unit* player = skills[0];
		int newHp = player->getHP() + player->useSkill();

		cout << "You use heal" << endl;

		player->setHP(newHp);
		player->displayStats();
	}
	else if (index == 2)
	{
		Unit* player = skills[1];
		int newPOW = player->getPOW() + player->useSkill();

		cout << "You use Might" << endl;

		player->setPOW(newPOW);
		player->displayStats();
	}
	else if (index == 3)
	{
		Unit* player = skills[2];
		int newDEX = player->getDEX() + player->useSkill();

		cout << "You use Concentration" << endl;

		player->setDEX(newDEX);
		player->displayStats();
	}
	else if (index == 4)
	{
		Unit* player = skills[3];
		int newVIT = player->getVIT() + player->useSkill();

		cout << "You use Ironskin" << endl;

		player->setVIT(newVIT);
		player->displayStats();
		
	}
	else if (index == 5)
	{
		Unit* player = skills[4];
		int newAGI = player->getAGI() + player->useSkill();

		cout << "You use Haste" << endl;

		player->setAGI(newAGI);
		player->displayStats();
	}
	else
	{
		return;
	}
}

int main()
{
	Unit *player = new Unit();
	Heal *heal = new Heal();
	Might *might = new Might();
	IronSkin *ironskin = new IronSkin();
	Concentration *concentration = new Concentration();
	Haste *haste = new Haste();

	srand(time(NULL));
	
	vector <Unit*> skills;

	skills.push_back(heal);
	skills.push_back(might);
	skills.push_back(concentration);
	skills.push_back(ironskin);
	skills.push_back(haste);
	


	while (true)
	{
		cout << "[1] Heal" << endl;
		cout << "[2] Might" << endl;
		cout << "[3] Concentration" << endl;
		cout << "[4] Ironskin" << endl;
		cout << "[5] Haste" << endl;

		int choice = rand() % 5 + 1;
		applySkill(skills, choice);
		//player->displayStats();
		_getch();
		system("cls");
	}

	
	
	_getch();
	return 0;
}