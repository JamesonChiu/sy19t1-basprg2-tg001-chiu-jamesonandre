#pragma once
#include <iostream>
#include <string>
#include "Unit.h"
using namespace std;

class IronSkin : public Unit
{
public:
	IronSkin();
	~IronSkin();
	
	int useSkill() override;

private:
	int mIronSkin;
};

