#pragma once
#include <iostream>
#include <string>
#include "Unit.h"
using namespace std;

class Might : public Unit
{
public:
	Might();
	~Might();

	int useSkill() override;

private:
	int mMight;
};

