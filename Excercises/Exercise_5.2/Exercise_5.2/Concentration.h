#pragma once
#include <iostream>
#include <string>
#include "Unit.h"
using namespace std;

class Concentration : public Unit
{
public:
	Concentration();
	~Concentration();
	
	int useSkill() override;

private:
	int mConcentration;
};

