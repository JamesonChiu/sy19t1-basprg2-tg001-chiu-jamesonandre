#pragma once
#include <iostream>
#include <string>
#include "Unit.h"
using namespace std;

class Heal : public Unit
{
public:
	Heal();
	~Heal();

	
	int useSkill() override;

private:
	int mHeal;
};


