#pragma once
#include <iostream>
#include <string>
#include "Unit.h"
using namespace std;

class Haste : public Unit
{
public:
	Haste();
	~Haste();
	

	int useSkill() override;

private:
	int mHaste;
};

