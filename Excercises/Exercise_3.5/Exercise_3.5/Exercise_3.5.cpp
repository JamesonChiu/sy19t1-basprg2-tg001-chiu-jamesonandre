// Exercise_3.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
using namespace std;

int Rand(int& RNG)
{
	RNG = rand() % 5;
	return RNG;
}

struct item
{
	string name;
	int gold;
};

void itemGenerate(item * Loot, int i)
{
	cout << Loot[i].name << " & " << Loot[i].gold << endl;
}

void enterDungeon(item * Loot, int * P_Gold)
{
	int RNG;
	int multiplier = 1;
	int stackGold = 0;
	bool inDungeon = true;
	char answer;

	cout << "-25 gold for dungeon fee" << endl;
	*P_Gold -= 25;


	while (inDungeon)
	{
		int RNG_Loot = Rand(RNG);

		cout << "Player Gold: " << *P_Gold << endl;
		cout << "Gold Grind: " << stackGold << endl;
		cout << "Gold Multiplier: " << multiplier << endl;
		_getch();
		system ("cls");

		cout << "You got: ";
		itemGenerate(Loot, RNG);
		_getch();

		stackGold += Loot[RNG].gold * multiplier;
		cout << "Current stack gold: " << stackGold << endl;
		_getch();

		if (RNG_Loot == 4)
		{
			cout << "You've been curse" << endl;
			cout << "Everthing you grind is now gone" << endl;
			cout << "You've been respawn outside the dungeon" << endl;
			inDungeon = false;
			_getch();
		}
		else
		{
			cout << "Do you still want to explore? (Y/N)" << endl;
			cin >> answer;
			switch (answer)
			{
			case 'Y':
			case 'y':
				if (multiplier < 4)
				{
					multiplier++;
				}
				break;
			case 'N':
			case 'n':
				cout << "You leave the dungeon" << endl;
				cout << "Manage to earn " << stackGold << endl;
				*P_Gold += stackGold;
				inDungeon = false;
				_getch();
			}
		}
	}

	_getch();
	system("cls");
}



int main()
{
	//--------------LOOT-LIST---------------------
	item* Loot = new item[5];
	Loot[0].name = "Mitril Ore";
	Loot[0].gold = 100;
	Loot[1].name = "Sharp Talon";
	Loot[1].gold = 50;
	Loot[2].name = "Thick Feater";
	Loot[2].gold = 25;
	Loot[3].name = "Jellopy";
	Loot[3].gold = 5;
	Loot[4].name = "Curse Stone";
	Loot[4].gold = 0;

	//---------------VARIABLES----------------------
	srand(time(NULL));
	
	int * playerGold = new int;
	*playerGold = 50;
	char Ans;
	bool Game = true;

	cout << "Welcome to the dungeon grind simulator" << endl;
	_getch();
	system("cls");

	while (Game)
	{
		

		cout << "Currently you have " << *playerGold << endl;
		enterDungeon(Loot, playerGold);
		_getch();
		
		if (*playerGold < 25)
		{
			cout << "Insuffiecient gold to enter" << endl;
			cout << "Thanks for playing" << endl;
			_getch();
			Game = false;
		}
		else if (*playerGold >= 500)
		{
			cout << "Congratulation you achieve the gold target" << endl;
			cout << "Thanks for playing" << endl;
			_getch();
			Game = false;
		}
		else
		{
			cout << "Do you want to explore the dungeon again? (Y/N)" << endl;
			cin >> Ans;
			switch (Ans)
			{
			case 'N':
			case 'n':
				cout << "You ended your journey without acomplishing anything...." << endl;
				Game = false;
			}
		}
	}
	delete[] Loot;
	delete[] playerGold;

	_getch();
	return 0;
}
