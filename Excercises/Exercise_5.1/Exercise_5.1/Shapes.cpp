#include "pch.h"
#include "Shapes.h"


Shapes::Shapes()
{
}


Shapes::~Shapes()
{
}

void Shapes::setName(string name)
{
	mName = name;
}

void Shapes::setNumSides(int numSides)
{
	mNumSides = numSides;
}

string Shapes::getName()
{
	return mName;
}

int Shapes::getNumSides()
{
	return mNumSides;
}

int Shapes::getArea()
{
	return 0;
}
