#pragma once
#include "Shapes.h"
#include <iostream>
#include <string>
using namespace std;

class Square : public Shapes
{
public:
	Square();
	~Square();

	int getLength();
	void setLength(int value);
	int getArea() override;

private:
	int mLenght;

};

