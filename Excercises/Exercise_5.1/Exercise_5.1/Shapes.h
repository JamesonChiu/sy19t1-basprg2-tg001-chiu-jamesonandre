#pragma once
#include <iostream>
#include <string>
using namespace std;


class Shapes
{
public:
	Shapes();
	~Shapes();

	void setName(string name);
	void setNumSides(int numSides);

	string getName();
	int getNumSides();

	virtual int getArea();
private:
	string mName;
	int mNumSides;
};

