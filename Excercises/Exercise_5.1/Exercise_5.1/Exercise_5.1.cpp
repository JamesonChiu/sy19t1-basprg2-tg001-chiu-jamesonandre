

#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
#include "Shapes.h"
#include "Square.h"
#include "Circle.h"
#include "Rectangle.h"
using namespace std;

void displayShape(Shapes* shape)
{
	cout << "Name: " << shape->getName() << endl;
	cout << "Number of Sides: " << shape->getNumSides() << endl;
	cout << "Area: " << shape->getArea() << endl;
}

void displayShape2(vector <Shapes*> shapes)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		Shapes* shape = shapes[i];
		cout << "Name: " << shape->getName() << endl;
		cout << "Number of Sides: " << shape->getNumSides() << endl;
		cout << "Area: " << shape->getArea() << endl;
		cout << endl;
	}
}
int main()
{
	vector <Shapes*> shapes;
	Square* square = new Square();
	Circle* circle = new Circle();
	Rectangle* rectangle = new Rectangle();
	
	shapes.push_back(square);
	shapes.push_back(circle);
	shapes.push_back(rectangle);

	int sMeasure;
	cout << "Input the length of square" << endl;
	cin >> sMeasure;
	square->setName("Square");
	square->setNumSides(4);
	square->setLength(sMeasure);

	int cMeasure;
	cout << "Input the radius of circle" << endl;
	cin >> cMeasure;
	circle->setName("Circle");
	circle->setNumSides(1);
	circle->setRadius(cMeasure);
	
	int rLength;
	int rWidth;
	cout << "Input the length of rectangle" << endl;
	cout << "Lenght: " << endl;
	cin >> rLength;
	cout << "Input the width of rectangle" << endl;
	cout << "Width: " << endl;
	cin >> rWidth;
	rectangle->setName("Rectangle");
	rectangle->setNumSides(4);
	rectangle->setLength(rLength);
	rectangle->setWidth(rWidth);

	system("cls");
	displayShape2(shapes);
	
}

