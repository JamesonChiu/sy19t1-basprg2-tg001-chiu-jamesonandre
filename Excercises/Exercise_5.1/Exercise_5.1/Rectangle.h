#pragma once
#pragma once
#include "Shapes.h"
#include <iostream>
#include <string>
using namespace std;

class Rectangle:public Shapes
{
public:
	Rectangle();
	~Rectangle();

	int getLength();
	void setLength(int value);
	int getWidth();
	void setWidth(int value);
	int getArea() override;

private:
	int mLenght;
	int mWidth;
};

