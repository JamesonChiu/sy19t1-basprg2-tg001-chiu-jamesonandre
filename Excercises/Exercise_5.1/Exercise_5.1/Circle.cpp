#include "pch.h"
#include "Circle.h"


Circle::Circle()
{
}


Circle::~Circle()
{
}

int Circle::getRadius()
{
	return mRadius;
}

void Circle::setRadius(int value)
{
	mRadius = value;
}

int Circle::getArea()
{
	return (mRadius * mRadius) * 3.14;
}
