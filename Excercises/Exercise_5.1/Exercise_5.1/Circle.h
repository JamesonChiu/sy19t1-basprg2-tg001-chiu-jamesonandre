#pragma once
#pragma once
#include "Shapes.h"
#include <iostream>
#include <string>
using namespace std;

class Circle: public Shapes
{
public:
	Circle();
	~Circle();

	int getRadius();
	void setRadius(int value);
	int getArea() override;

private:
	int mRadius;
};

