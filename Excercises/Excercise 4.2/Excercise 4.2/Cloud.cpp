#include "pch.h"
#include "Cloud.h"


Cloud::Cloud()
{
	 name = "Cloud";
	 mHP = 100;
	 mDMG = 10;
	 mDEF = 10;

}


Cloud::~Cloud()
{
	delete mWeapon;
}

int Cloud::getHP()
{
	return mHP;
}

void Cloud::setHP(int value)
{
	mHP = value;
}

int Cloud::getDMG()
{
	return mDMG;
}

int Cloud::getDEF()
{
	return mDEF;
}


void Cloud::defence(int value)
{
	mDEF = value;
}

void Cloud::attack(Cloud * target)
{
	int damage = mDMG - target->mDEF;
	target->mHP = -damage;
}

Equipment * Cloud::setWeapon()
{
	return mWeapon;
}

void Cloud::equipWeapon(Equipment * weapon)
{
	if (mWeapon != nullptr)
	{
		delete mWeapon;
	}

	mWeapon = weapon;
}


