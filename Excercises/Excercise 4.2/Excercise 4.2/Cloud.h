#pragma once
#include <string>
#include <iostream>
#include "Equipment.h"
using namespace std;

class Cloud
{
public:
	Cloud();
	~Cloud();

	int getHP();
	void setHP(int value);
	int getDMG();
	int getDEF();
	void defence(int value);
	void attack(Cloud * target);
	

	Equipment* setWeapon();
	void equipWeapon(Equipment* weapon);

private:

	string name;
	int mHP;
	int mDMG;
	int mDEF;
	Equipment* mWeapon;

	

};
