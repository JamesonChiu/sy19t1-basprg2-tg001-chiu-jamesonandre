

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
using namespace std;

void arrayPointer(int *Array)
{
	srand(time(NULL));

	for (int i = 0; i < 10; i++)
	{
		Array[i] = rand() % 100;
		cout << Array[i] << " ";
	}

	cout << endl;
}

int * dynamicArray()
{
	int* Array2;
	Array2 = new int[10];

	srand(time(NULL));

	for (int i = 0; i < 10; i++)
	{
		Array2[i] = rand() % 100;
		cout << Array2[i] << " ";
	}

	cout << endl;

	return Array2;
}

int dynamicDelete(int *Array,int size)
{
	srand(time(NULL));

	for (int i = 0; i < size; i++)
	{
		Array[i] = rand() % 100;
		cout << Array[i] << " ";
	}

	cout << endl;

	return *Array;
}


int main()
{
	// 3-1 Array to Pointer

	int Array[10] = {};
	int *numbers = Array;

	arrayPointer(numbers);

	// 3-2 Dynamic
	dynamicArray();

	// 3-3 Delete dynamic elements
	int size;
	int* Array1;
	Array1 = new int[100];
	int* Array2;
	Array2 = new int[100];

	cout << "Array size:" << endl;
	cin >> size;

	dynamicDelete(Array1, size);

	delete[] Array1; // Cant access Array1 since its already been deleted
	_getch();
	
	cout << "Array size:" << endl;
	cin >> size;

	dynamicDelete(Array2, size);

	delete[] Array2; // Cant access Array2 since its already been deleted

	
	_getch();
	return 0;
}

