

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
using namespace std;


int betGold(int& playerGold) 
{
	int newGold;
	int bet;

	cout << "Current Gold " << playerGold << endl;
	cout << "Place your bet" << endl;
	cin >> bet;

	playerGold = playerGold - bet;
	return playerGold;
}

void betGold(int& playerGold, int& bet) 
{
	
	cout << "Current Gold " << playerGold << endl;
	cout << "Place your bet" << endl;
	cin >> bet;

	if (bet == 0 || bet > playerGold)
	{
		cout << "Invalid Bet" << endl;
		_getch();
		system("cls");
	}

	playerGold = playerGold - bet;
}

int diceRoll(int& dice, int& dice2)
{
	dice = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
	int totalDice;
	
	totalDice = dice + dice2;	
	if (totalDice == 2)
	{
		cout << "You got Snake Eyes" << endl;
	}
	return totalDice;
}

void playRound(int& playerGold, int bet,int total, int Aitotal)
{
	

		cout << "Let the dice game begins" << endl;
		_getch();
		system("cls");


		cout << "Player Turn" << endl;
		cout << "Rolled a total of " << total << endl;
		
		_getch();
		system("cls");



		cout << "AI Turn" << endl;
		cout << "Rolled a total of " << Aitotal << endl;
		if (Aitotal == 2)
		{
			cout << "Ai got snake eye" << endl;
		}
		_getch();
		system("cls");
	
}

void payout(int& playerGold, int bet, int total, int AiTotal)
{

	if (total == 2)
	{
		cout << "Insta Win" << endl;
		playerGold = playerGold + (bet * 3);
	}
	else if (AiTotal == 2)
	{
		cout << "Insta Lose" << endl;
		playerGold = playerGold + (bet * 2);
	}

	else if (total > AiTotal)
	{

		cout << "Player won" << endl;
		playerGold = playerGold + (bet * 2);
	}

	else if (total < AiTotal)
	{
		cout << "AI won" << endl;
	}

	else
	{
		cout << "It's a draw" << endl;
		playerGold = playerGold + bet;
	}

	_getch();
	system("cls");
}

int main()
{
	srand(time(NULL));
	int playerGold = 1000;
	int bet;
	int dice;
	int dice2;
	bool x = true;
	char ans;


	while (x)
	{
		int Total = diceRoll(dice, dice2);
		int AiTotal = diceRoll(dice, dice2);

		betGold(playerGold, bet);


		playRound(playerGold, bet, Total, AiTotal);


		payout(playerGold, bet, Total, AiTotal);
		

		if (playerGold <= 0)
		{
			cout << "Game Over" << endl;
			cout << "You ran out of Gold" << endl;
			x = false;
			return 0;
		}

		cout << "Continue?(Y/N)" << endl;
		cin >> ans;
		switch (ans)
		{
		case 'N':
		case 'n':
			cout << "Thank you for playing" << endl;
			x = false;
		}

		system("cls");
	}
	


	_getch();
	return 0;
}

/*cout << "Let the dice game begins" << endl;
		_getch();
		system("cls");


		cout << "Player Turn" << endl;
		int Total = diceRoll(dice, dice2);
		cout << "Rolled a total of " << Total << endl;
		if (dice == dice2)
		{
			cout << "It's a snake eye" << endl;
		}
		_getch();
		system("cls");



		cout << "AI Turn" << endl;
		int AiTotal = diceRoll(dice, dice2);
		cout << "Rolled a total of " << AiTotal << endl;
		if (dice == dice2)
		{
			cout << "It's a snake eye" << endl;
		}
		_getch();
		system("cls");*/