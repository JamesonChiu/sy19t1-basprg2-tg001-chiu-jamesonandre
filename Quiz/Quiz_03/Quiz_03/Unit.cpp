#include "pch.h"
#include "Unit.h"


Unit::Unit()
{
}


Unit::~Unit()
{
	
}

Unit::Unit(string name, int MaxHP, int HP, int POW, int VIT, int AGI, int DEX)
{
	mName = name;
	mMaxHP = MaxHP;
	mHP = HP;
	mPOW = POW;
	mVIT = VIT;
	mAGI = AGI;
	mDEX = DEX;
}

string Unit::getName()
{
	return mName;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getMaxHP()
{
	return mMaxHP;
}

int Unit::getPOW()
{
	return mPOW;
}

int Unit::getVIT()
{
	return mVIT;
}

int Unit::getAGI()
{
	return mAGI;
}

int Unit::getDEX()
{
	return mDEX;
}

void Unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHP << "/" << mMaxHP << endl;
	cout << "POW: " << mPOW << endl;
	cout << "VIT: " << mVIT << endl;
	cout << "AGI: " << mAGI << endl;
	cout << "DEX: " << mDEX << endl;
}

void Unit::takeDamage(int damage)
{
	if (damage < 1)
	{
		return;
	}
	mHP -= damage;
	if (mHP < 0)
	{
		mHP = 0;
	}
}

void Unit::attack(Unit * target)
{
	int hitRate = (this->getDEX() / target->getAGI()) * 100;
	if (hitRate >= 20 || hitRate <= 80)
	{
		int DMG = (this->getPOW() - target->getVIT());
		if (DMG < 0)
		{
			DMG = 1;
		}
		cout << DMG << " damage" << endl;
		target->takeDamage(DMG);

	}
	else 
	{
		cout << "Attack Miss." << endl;
		return;
	}
}

void Unit::WarriorRewards()
{
	mHP += mMaxHP * 0.3;
	mMaxHP += 3;
	mVIT += 3;
}

void Unit::AssasinRewards()
{
	mHP += mMaxHP * 0.3;
	mAGI += 3;
	mDEX += 3;
}

void Unit::MageRewards()
{
	mHP += mMaxHP * 0.3;
	mPOW += 5;
}



void Unit::enemyUP()
{

	mMaxHP *= 2;
	mHP = mMaxHP;
	mPOW *= 2;
	mVIT *= 2;
	mAGI *= 2;
	mDEX *= 2;
}
