
#include "pch.h"
#include <iostream>
#include <time.h>
#include <string>
#include <conio.h>
#include <vector>
#include "Unit.h"

using namespace std;

void pClassList(vector <string>& playerClass)
{
	playerClass.push_back("Warrior");
	playerClass.push_back("Assasin");
	playerClass.push_back("Mage");

}

void eClassList(vector <string>& enemyClass)
{
	enemyClass.push_back("Warrior");
	enemyClass.push_back("Assasin");
	enemyClass.push_back("Mage");

}

void p_pickJob(vector <string>& playerClass, int choice)
{
	cout << "Class: " << playerClass[choice] << endl;;

}

void e_pickJob(vector <string>& enemyClass, int RNG)
{
	cout << "Class: " << enemyClass[RNG] << endl;;
}

void fight(Unit* player, Unit* enemy)
{
	if (player->getAGI() > enemy->getAGI())
	{
		cout << player->getName() << " deals ";
		player->attack(enemy);
		_getch();
		cout << enemy->getName() << " deals ";
		enemy->attack(player);
	}
	else
	{
		cout << enemy->getName() << " deals ";
		enemy->attack(player);
		_getch();
		cout << player->getName() << " deals ";
		player->attack(enemy);
	}
}

void rewards(Unit* player, vector <string>& eclassList, int RNG)
{
	if (eclassList[RNG] == "Warrior")
	{
		player->WarriorRewards();
	}
	else if (eclassList[RNG] == "Assasin")
	{
		player->AssasinRewards();
	}
	else if (eclassList[RNG] == "Mage")
	{
		player->MageRewards();
	}
	else
	{
		cout << "Nothing..." << endl;
	}
}

int main()
{
	srand(time(NULL));

	vector<string> pJobClass;
	vector <string> eJobClass;

	pClassList(pJobClass);
	eClassList(eJobClass);

	int pPOW = rand() % 11 + 1;
	int pVIT = rand() % 11 + 1;
	int pAGI = rand() % 11 + 1;
	int pDEX = rand() % 11 + 1;


	int ePOW = rand() % 11 + 1;
	int eVIT = rand() % 11 + 1;
	int eAGI = rand() % 11 + 1;
	int eDEX = rand() % 11 + 1;

	string name;
	cout << "Input Name" << endl;
	cin >> name;
	
	int choice;
	cout << "Pick a class: " << endl;
	cout << "[1] Warrior" << endl;
	cout << "[2] Assasin" << endl;
	cout << "[3] Mage" << endl;
	cin >> choice;
	choice--;

	Unit* player = new Unit(name, 20, 20, pPOW, pVIT, pAGI, pDEX);
	

	int rounds = 1;

	while (player->getHP() > 0)
	{
		Unit* enemy = new Unit("Bot", 10, 10, ePOW, eVIT, eAGI, eDEX);
	
		if (rounds > 1)
		{
			enemy->enemyUP();
		}

		player->printStats();
		p_pickJob(pJobClass, choice);
		cout << "VS" << endl;
		enemy->printStats();
		int RNG = rand() % 3;
		e_pickJob(eJobClass, RNG);
		system("pause");

		bool x = true;
		while (x)
		{
			fight(player, enemy);

			
			if (enemy->getHP() <= 0)
			{
				cout << "You defeated the enemy" << endl;
				_getch();
				system("cls");
				rewards(player, eJobClass, RNG);
				cout << "Your stats been upgraded" << endl;
				player->printStats();
				delete enemy;
				x = false;
			}
			if (player->getHP() <= 0)
			{
				cout << "Game Over" << endl;
				cout << "You survived " << rounds << " rounds" << endl;
				x = false;
			}

			_getch();
		}

		x = true;
		rounds += 1;
		system("pause");
		system("cls");
	}
}

