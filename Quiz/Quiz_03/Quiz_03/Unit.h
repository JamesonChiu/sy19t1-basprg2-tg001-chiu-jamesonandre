#pragma once
#include <iostream>
#include <string>
using namespace std;


class Unit
{
public:
	Unit();
	~Unit();
	Unit(string name, int MaxHP, int HP, int POW, int VIT, int AGI, int DEX);

	//Getters
	string getName();
	int getHP();
	int getMaxHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();

	void printStats();
	
	void takeDamage(int damage);

	void attack(Unit* target);

	void WarriorRewards();
	void AssasinRewards();
	void MageRewards();

	void enemyUP();

private:
	string mName;
	int mMaxHP;
	int mHP;
	int mPOW;
	int mVIT;
	int mAGI;
	int mDEX;

};

