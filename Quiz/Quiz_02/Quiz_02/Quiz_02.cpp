// Quiz_02.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
using namespace std;

#ifndef NODE_H
#define NODE_H
struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};
#endif //NODE_H

Node* linkList() // to access the content of this memory
{
	Node* start = new Node; //starting point of the link list
	Node* back = start; 
	for (int i = 1; i < 5;i++)
	{
		Node* node = new Node; 
		back->next = node; //creates a new node
		back = back->next; //connects the previous 
	}
	back->next = start; // connect the strting node
	return start;
}

Node* inputNode(Node* list, string nameInput)
{
	list->name = nameInput; 
	list = list->next; 

	return list; 
}

Node* removeNode(Node* list, int remover)
{
	Node *back = new Node;
	for (int i = 0; i < remover; i++)
	{
		back = list;
		list = list->next;
	}

	back->next = list->next;
	delete list; // deletes the specific node
	back = back->next;
	return back;

}

void printRemoved(Node* list, int remover)
{
	Node *temp = list;
	for (int i = 0; i < remover; i++)
	{
		temp = temp->next;
	}
	cout << temp->name << " will stay...";
}

void printList(Node* list, int availablePlayers)
{
	for (int i = 0; i < availablePlayers; i++)
	{
		cout << list->name << endl;
		list = list->next;
	}
}

int main()
{
	srand(time(NULL));
	Node *players = new Node;
	int playerNumber = 5;
	int round = 1;
	string name;
	bool x = true;

	cout << "THE WALL" << endl;
	cout << "The last 5 survivors edition" << endl;
	_getch();
	system("cls");

	players = linkList(); // creates a new Node(struct) memory named players which contains a new link list
	
	//Input name for the link list
	cout << "Input names to determine who will go" << endl;
	for (int i = 0; i < 5;i++)
	{
		cin >> name;
		players = inputNode(players, name); 
	}
	_getch();
	system("cls");
	
	//Mechanics
	cout << "Let the game begins" << endl;
	_getch();
	system("cls");
	

	while (x)
	{
		cout << "Round " << round << endl;
		cout << "Here's the list of players:" << endl;
		printList(players, playerNumber); // Prints the players
		_getch();

		int remover = rand() % 5 + 1;
		cout << "Result: " << endl;
		cout << players->name << " choose " << remover << endl; 
		printRemoved(players, remover); // Prints who will be eliminated
		players = removeNode(players, remover);

		playerNumber--;
		round++;

		if (playerNumber == 1)
		{
			x = false;
		}

		_getch();
		system("cls");
	}

	cout << "CONCLUSION" << endl;
	cout << players->name << " has been chosen to get some help" << endl;
	cout << "TO BE CONTINUE..." << endl;
	
	_getch();
	return 0;
}