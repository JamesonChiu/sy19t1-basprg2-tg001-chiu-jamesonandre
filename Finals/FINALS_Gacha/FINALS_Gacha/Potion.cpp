#include "pch.h"
#include "Potion.h"


Potion::Potion(string name, int healValue) : Items(name)
{
	heal = healValue;
}

Potion::~Potion()
{
}

void Potion::gachaRoll(Unit * target)
{
	Items::gachaRoll(target);

	target->heal(heal);
	cout << "Got Potion, You've been heal for " << heal << " HP" << endl;
}
