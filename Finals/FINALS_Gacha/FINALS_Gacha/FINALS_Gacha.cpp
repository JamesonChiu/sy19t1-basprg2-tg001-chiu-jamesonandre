#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "Unit.h"
#include "Potion.h"
#include "Crystal.h"
#include "Bomb.h"
#include "Rare.h"
#include "SuperRare.h"
#include "SuperSuperRare.h"
#include "Inventory.h"
using namespace std;

Unit * create()
{
	Unit* unit = new Unit("Player", 100, 100, 0);

	Items* superSuperRare = new SuperSuperRare("SSR", 50);
	unit->addItem(superSuperRare);

	Items* superRare = new SuperRare("SR", 10);
	unit->addItem(superRare);

	Items* rare = new Rare("R", 1);
	unit->addItem(rare);

	Items* potion = new Potion("Potion", 30);
	unit->addItem(potion);

	Items* bomb = new Bomb("Bomb", 25);
	unit->addItem(bomb);

	Items* crystal = new Crystal("Crystal", 15);
	unit->addItem(crystal);

	return unit;
}

Inventory * inventory()
{
	Inventory* inventory = new Inventory("Result", 0, 0, 0, 0, 0, 0);

	return inventory;
}

void gameplay(Unit* player, int RNG, Inventory* score)
{
	if (RNG == 1)
	{
		Items* pull = player->getItems()[0];
		pull->gachaRoll(player);

		score->SSRScore();
	}
	else if (RNG <= 10) // 2 to 10
	{
		Items* pull = player->getItems()[1];
		pull->gachaRoll(player);

		score->SRScore();
	}
	else if (RNG <= 40) // 11 to 40
	{
		Items* pull = player->getItems()[2];
		pull->gachaRoll(player);

		score->RScore();
	}
	else if (RNG <= 55) // 41 to 55
	{
		Items* pull = player->getItems()[3];
		pull->gachaRoll(player);

		score->PotionScore();
	}
	else if (RNG <= 75) // 56 to 75
	{
		Items* pull = player->getItems()[4];
		pull->gachaRoll(player);

		score->BombScore();
	}
	else if (RNG <= 100) // 76 to 100
	{
		Items* pull = player->getItems()[5];
		pull->gachaRoll(player);

		score->CrystalScore();
	}
}

int main()
{
	srand(time(NULL));

	Unit* player = create();
	Inventory* score = inventory();

	bool x = true;
	int rolls = 0;
	while (x)
	{

		if (player->getCrystal() <= 0 || player->getRarityPoints() >= 100)
		{
			x = false;
		}

		player->displayStats();
		cout << "Rolls: " << rolls << endl;
		_getch();
		cout << endl;

		player->payCrystal();
		int RNG = rand() % 100 + 1;

		gameplay(player, RNG, score);

		rolls += 1;
		if (player->getHP() <= 0)
		{
			x = false;
		}
		system("pause");
		system("cls");
	}

	if (player->getRarityPoints() >= 100)
	{
		cout << "Congratulation, you win the game" << endl;
	}
	else
	{
		cout << "Game Over" << endl;
	}

	cout << "Rolls: " << rolls << endl;
	score->printScore();
	cout << "RarityPoints: " << player->getRarityPoints() << endl;


	delete player;
	system("pause");
	return 0;
}