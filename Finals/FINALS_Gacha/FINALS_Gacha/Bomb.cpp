#include "pch.h"
#include "Bomb.h"

Bomb::Bomb(string name, int damage) : Items(name)
{
	dmg = damage;
}

Bomb::~Bomb()
{
}

void Bomb::gachaRoll(Unit * target)
{
	Items::gachaRoll(target);

	target->bomb(dmg);
	cout << "You got a bomb, deals " << dmg << " damage" << endl;
}
