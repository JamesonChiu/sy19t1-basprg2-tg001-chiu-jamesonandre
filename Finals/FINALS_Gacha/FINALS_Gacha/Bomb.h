#pragma once
#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class Bomb : public Items
{
public:
	Bomb(string name, int damage);
	~Bomb();

	void gachaRoll(Unit* target);

private:
	int dmg;
};