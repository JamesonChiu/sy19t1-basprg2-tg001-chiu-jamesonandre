#include "pch.h"
#include "SuperRare.h"

SuperRare::SuperRare(string name, int point) : Items(name)
{
	mPoint = point;
}

SuperRare::~SuperRare()
{
}

void SuperRare::gachaRoll(Unit * target)
{
	Items::gachaRoll(target);

	target->SRPoint(mPoint);
	cout << "You got a SR, grant " << mPoint << " point" << endl;
}
