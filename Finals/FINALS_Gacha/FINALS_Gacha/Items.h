#pragma once
#include <iostream>
#include <vector>
#include "Unit.h"
using namespace std;

class Unit;

class Items
{
public:
	Items(string name);
	~Items();

	string getName();
	Unit* getActor();
	void setActor(Unit* actor);



	virtual void gachaRoll(Unit* target);

	
private:
	string mName;
	Unit* mActor;

};
