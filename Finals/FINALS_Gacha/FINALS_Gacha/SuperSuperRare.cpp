#include "pch.h"
#include "SuperSuperRare.h"


SuperSuperRare::SuperSuperRare(string name, int point) : Items(name)
{
	mPoint = point;
}

SuperSuperRare::~SuperSuperRare()
{
}

void SuperSuperRare::gachaRoll(Unit * target)
{
	Items::gachaRoll(target);

	target->SSRPoint(mPoint);
	cout << "You got a SSR, grant " << mPoint << " point" << endl;
}
