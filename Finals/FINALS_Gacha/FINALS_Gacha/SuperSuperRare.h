#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class SuperSuperRare : public Items
{
public:
	SuperSuperRare(string name, int point);
	~SuperSuperRare();

	void gachaRoll(Unit* target);


private:
	int mPoint;
};
