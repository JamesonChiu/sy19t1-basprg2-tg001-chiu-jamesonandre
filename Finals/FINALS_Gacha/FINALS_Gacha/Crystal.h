#pragma once
#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class Crystal : public Items
{
public:
	Crystal(string name, int addCrystal);
	~Crystal();

	void gachaRoll(Unit* target);

private:
	int mCrystal;

};

