#pragma once
#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class Rare : public Items
{
public:
	Rare(string name, int point);
	~Rare();

	void gachaRoll(Unit* target);

private:
	int mPoint;
};


