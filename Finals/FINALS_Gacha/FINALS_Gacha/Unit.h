#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Items.h"
using namespace std;

class Items;

class Unit
{
public:
	Unit(string name, int HP, int Crystal, int raritypoints);
	~Unit();

	//GETTERS
	string getName();
	int getHP();
	int getCrystal();
	int getRarityPoints();

	void payCrystal();

	vector<Items*>& getItems();
	void addItem(Items * item);

	//Effects
	void heal(int plusHP);
	void addCrystal(int plusCrystal);
	void bomb(int dmg);
	void RPoint(int points);
	void SRPoint(int points);
	void SSRPoint(int points);


	void displayStats();


private:
	string mName;
	int mHP;
	int mCrystal;
	int mRarityPoints;
	vector <Items*> mItem;

};

