#include "pch.h"
#include "Inventory.h"


Inventory::Inventory(string name, int SSR, int SR, int R, int potion, int bomb, int crystal) : Items(name)
{
	mSSR = SSR;
	mSR = SR;
	mR = R;
	mPotion = potion;
	mBomb = bomb;
	mCrystal = crystal;
}

Inventory::~Inventory()
{
}


void Inventory::RScore()
{
	mR += 1;
}

void Inventory::SRScore()
{
	mSR += 1;
}

void Inventory::SSRScore()
{
	mSSR += 1;
}

void Inventory::PotionScore()
{
	mPotion += 1;
}

void Inventory::BombScore()
{
	mBomb += 1;
}

void Inventory::CrystalScore()
{
	mCrystal += 1;
}

void Inventory::printScore()
{
	cout << "SSR: " << mSSR << endl;
	cout << "SR: " << mSR << endl;
	cout << "R: " << mR << endl;
	cout << "Potion: " << mPotion << endl;
	cout << "Bomb: " << mBomb << endl;
	cout << "Crystal: " << mCrystal << endl;
}
