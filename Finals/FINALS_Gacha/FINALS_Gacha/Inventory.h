#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class Inventory : public Items
{
public:
	Inventory(string name, int SSR, int SR, int R, int potion, int bomb, int crystal);
	~Inventory();

	void RScore();
	void SRScore();
	void SSRScore();
	void PotionScore();
	void BombScore();
	void CrystalScore();

	void printScore();
private:
	int mR;
	int mSR;
	int mSSR;
	int mPotion;
	int mBomb;
	int mCrystal;
};
