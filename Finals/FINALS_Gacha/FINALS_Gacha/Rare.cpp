#include "pch.h"
#include "Rare.h"


Rare::Rare(string name, int point) : Items(name)
{
	mPoint = point;
}

Rare::~Rare()
{
}

void Rare::gachaRoll(Unit * target)
{
	Items::gachaRoll(target);

	target->RPoint(mPoint);
	cout << "You got a R, grant " << mPoint << " point" << endl;
}
