#pragma once
#include <string>
#include <iostream>
#include "Items.h"
using namespace std;

class Potion : public Items
{
public:
	Potion(string name, int healValue);
	~Potion();

	void gachaRoll(Unit* target);

private:
	int heal;
};
