#pragma once
#include "Items.h"
#include <string>
#include <iostream>
using namespace std;

class SuperRare : public Items
{
public:
	SuperRare(string name, int point);
	~SuperRare();

	void gachaRoll(Unit* target);

private:
	int mPoint;
};
