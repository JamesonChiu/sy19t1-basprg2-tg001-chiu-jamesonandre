#include "pch.h"
#include "Unit.h"



Unit::Unit(string name, int HP, int Crystal, int raritypoints)
{
	mName = name;
	mHP = HP;
	mCrystal = Crystal;
}

Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getCrystal()
{
	return mCrystal;
}

int Unit::getRarityPoints()
{
	return mRarityPoints;
}

void Unit::payCrystal()
{
	mCrystal -= 5;
}

vector<Items*>& Unit::getItems()
{
	return mItem;
}

void Unit::addItem(Items * item)
{
	item->setActor(this);

	mItem.push_back(item);
}

void Unit::heal(int plusHP)
{
	mHP += plusHP;

	if (mHP > 100)
	{
		mHP = 100;
	}

}

void Unit::addCrystal(int plusCrystal)
{
	mCrystal += 15;
}

void Unit::bomb(int dmg)
{
	mHP -= dmg;

	if (mHP < 0)
	{
		mHP = 0;
	}
}

void Unit::RPoint(int points)
{
	mRarityPoints += 1;
}

void Unit::SRPoint(int points)
{
	mRarityPoints += 10;
}

void Unit::SSRPoint(int points)
{
	mRarityPoints += 50;
}


void Unit::displayStats()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHP << endl;
	cout << "Crstals: " << mCrystal << endl;
	cout << "RarityPoints: " << mRarityPoints << endl;
}