// MIDTERMS_ECard.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include <vector>
#include <time.h>
#include <string>
using namespace std;

void DeckOfCards(vector<string>& Cards, int rounds) // Kaiji Card Deck
{
	if (rounds <= 3 || (rounds >= 7 && rounds <= 9))
	{
		Cards.push_back("Emperor");
	}
	else
	{
		Cards.push_back("Slave");
	}

	Cards.push_back("Civilian");
	Cards.push_back("Civilian");
	Cards.push_back("Civilian");
	Cards.push_back("Civilian");
}

void EnemyDeckOfCards(vector<string>& EnemyCards, int rounds) //Enemy Card Deck
{
	if (rounds <= 3 || (rounds >= 7 && rounds <= 9))
	{
		EnemyCards.push_back("Slave");
	}
	else
	{
		EnemyCards.push_back("Emperor");
	}

	EnemyCards.push_back("Civilian");
	EnemyCards.push_back("Civilian");
	EnemyCards.push_back("Civilian");
	EnemyCards.push_back("Civilian");
}

void playerPrintDeck(vector<string>& playerCards)
{
	for (int i = 0; i < playerCards.size(); i++)
	{
		cout << "[" << i + 1 << "]" << playerCards[i] << endl;
	}
}

void pickCard(vector<string>& playerCards, int pick)
{
	cout << "You Pick " << playerCards[pick] << endl;
	_getch();
}

void enemyPick(vector<string>& enemyCards, int RNG)
{
	cout << "Your opponent pick " << enemyCards[RNG] << endl;
	_getch();
	
}

void result(vector<string>& playerCards, vector<string>& enemyCards, int pick, int RNG, int& Money, int& Distance, int Bet, bool& x2)
{
	if (playerCards[pick] == "Emperor" && enemyCards[RNG] == "Civilian")
	{
		cout << "Player Wins" << endl;
		Money += 100000;
		_getch();
		x2 = 0;
	}
	else if (playerCards[pick] == "Civilian" && enemyCards[RNG] == "Slave")
	{
		cout << "Player Wins" << endl;
		Money += 100000;
		_getch();
		x2 = 0;
	}
	else if (playerCards[pick] == "Emperor" && enemyCards[RNG] == "Slave")
	{
		cout << "Oppenent Wins" << endl;
		Distance -= Bet;
		_getch();
		x2 = 0;
	}
	else if (playerCards[pick] == "Slave" && enemyCards[RNG] == "Emperor")
	{
		cout << "Player Wins" << endl;
		Money += 500000;
		_getch();
		x2 = 0;
	}
	else if (playerCards[pick] == "Slave" && enemyCards[RNG] == "Civilian")
	{
		cout << "Oppenent Wins" << endl;
		Distance -= Bet;
		_getch();
		x2 = 0;
	}
	else if (playerCards[pick] == "Civilian" && enemyCards[RNG] == "Emperor")
	{
		cout << "Oppenent Wins" << endl;
		Distance -= Bet;
		_getch();
		x2 = 0;
	}
	else
	{
		cout << "It's a tie" << endl;
		_getch();
		playerCards.erase(playerCards.begin() + pick);
		enemyCards.erase(enemyCards.begin() + RNG);
	}
}

void conclusion(int Money, int Distance, bool& x)
{
	if (Money >= 20000000 && Distance > 0)
	{
		cout << "Congratulations you win the game safe and sound..." << endl;
	}
	else if (Money <= 20000000 && Distance > 0)
	{
		cout << "Meh, didnt win completely..." << endl;
		x = 0;
	}
	else 
	{
		cout << "Too bad, you lost the game and your hearing..." << endl;
		x = 0;
	}
}

int main()
{
	//CARDS
	vector<string> playerCards;
	vector<string> enemyCards;
	//VARIABLE
	srand(time(NULL));
	int Rounds = 1;
	int Money = 0;
	int Distance = 30;
	int Draw;
	int Bet;
	int pick;
	bool x = 1;
	bool x2 = 1;
	
	cout << "Welcome to E-CARD game" << endl;
	while (x)
	{
		//MAIN GAMEPLAY
		DeckOfCards(playerCards, Rounds);
		EnemyDeckOfCards(enemyCards, Rounds);
		

		cout << "DETAILS:" << endl;
		cout << "Round: " << Rounds << endl;
		cout << "Distance: " << Distance << endl;
		cout << "Money: " << Money << endl;
		system("pause");
		system("cls");

		cout << "Place your bet: " << endl;
		cin >> Bet;

		if (Bet > Distance || Bet <= 0)
		{
			cout << "Invalid Bet" << endl;
			_getch();
			Rounds--;
			x2 = 0;
		}

		while (x2)
		{
			int RNG = rand() % enemyCards.size();

			cout << "Pick your Card" << endl;
			playerPrintDeck(playerCards);
			cin >> pick;
			pick--;

			pickCard(playerCards, pick);
			enemyPick(enemyCards, RNG);

			result(playerCards, enemyCards,pick, RNG, Money, Distance, Bet, x2);
		}

		if (Rounds >= 12 || Distance <= 0)
		{
			system("cls");
			_getch();
			conclusion(Money, Distance, x);
			_getch();
			x = 0;
		}

		Rounds++;
		x2 = 1;
		system("cls");
		playerCards.clear();
		enemyCards.clear();
	}

	cout << "Thanks For playing" << endl;
	_getch();
	return 0;
}
